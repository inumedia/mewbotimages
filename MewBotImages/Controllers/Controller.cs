﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;

namespace MewBotImages.Controllers
{
    [Route("/")]
    [ApiController]
    public class Controller : ControllerBase
    {
        static string endPoint = Environment.GetEnvironmentVariable("REMOTE_ADDR");
        static HttpClient client = new HttpClient()
        {
            BaseAddress = new Uri(endPoint)
        };
        // GET api/values
        [HttpGet]
        [Route("/{*path}")]
        public async Task<IActionResult> Get(string path)
        {
            for (int i = 0; i < 5; ++i)
            {
                try
                {
                    using (var message = await client.GetAsync(path))
                    {
                        string contentType = "image/png";
                        if (message.Headers.TryGetValues("Content-Type", out var ct) && ct.Count() > 0)
                            contentType = ct.First();

                        byte[] data = await message.Content.ReadAsByteArrayAsync();

                        long now = DateTime.Now.Ticks;
                        using (Image<Rgba32> img = Image.Load(data))
                        {
                            int y = (int)(((now / img.Width) + 1) % img.Height);
                            int x = (int)(now % img.Width) + 1;
                            var pixel = img[x, y];

                            if (pixel.A < 127)
                                pixel.A++;
                            else
                                pixel.A--;

                            img[x, y] = pixel;

                            using (MemoryStream mem = new MemoryStream())
                            {
                                img.Save(mem, PngFormat.Instance);
                                byte[] result = mem.ToArray();
                                return File(result, contentType);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (i >= 4)
                        throw;
                }
            }

            throw new InvalidOperationException();
        }
    }
}
