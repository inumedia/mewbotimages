FROM microsoft/dotnet:2.2-aspnetcore-runtime

WORKDIR /app
COPY MewBotImages/build .
CMD ["dotnet", "MewBotImages.dll"]